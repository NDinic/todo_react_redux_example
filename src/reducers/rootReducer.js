
const initState = {
  post: [],
  todo: [
    { id: 1, content: 'todo initState 1' },
    { id: 2, content: 'todo initDtate 2' },
    { id: 3, content: 'todo initState 3' },
    { id: 4, content: 'todo initDtate 4' }
  ]
}

const rootReducer = (state = initState, action) => {
  if(action.type === 'DELETE_TODO') {
    let delTodo = state.todo.filter( todo => {
       return action.id !== todo.id
    });
    return {
      ...state,
      todo: delTodo
    }
  }
  if(action.type === 'ADD_TODO') {
    let newTodo = [...state.todo];
    newTodo.unshift(action.todo);
    return {
      ...state,
      todo: newTodo
    }
  }
  return state;
}

export default rootReducer;