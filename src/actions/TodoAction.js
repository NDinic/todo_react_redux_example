import * as actionTypes from './actionTypes';

export const addTodoAction = (todo) => {
  return {
    type: actionTypes.ADD_TODO,
    todo
  }
}

export const delTodoAction = (id) => {
  return {
    id: id,
    type: actionTypes.DEL_TODO
  }
}