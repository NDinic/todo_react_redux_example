import React, { Component } from 'react'

class AddTodo extends Component {
  state = {
    content: ''
  }
  changeHandler = (e) => {
    this.setState({ content: e.target.value })
  }
  submitHandler = (e) => {
    e.preventDefault();
    this.props.addTodo(this.state)
    this.setState({ content: '' })
  }
  render() {
    return (
      <div>
        <form onSubmit={this.submitHandler}>
          <label>Add new todo:</label>
          <input type="text" name="" value={this.state.content} onChange={this.changeHandler} />
        </form>
      </div>
    )
  }
}

export default AddTodo;