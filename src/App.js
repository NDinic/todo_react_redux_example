import React, { Component } from 'react';
import Todos from './Todos';
import AddTodo from './AddTodo';
import { connect } from 'react-redux';
import { addTodoAction } from './actions/TodoAction';
import { delTodoAction } from './actions/TodoAction';

class App extends Component {
  // state = {
  //   todos: [
  //     { id: 1, content: 'todo 1' },
  //     { id: 2, content: 'todo 2' }
  //   ]
  // }
  
  deleteTodo = (id) => {
    // console.log(id)
    this.props.deleteTodo(id)
    // const todos = this.props.todos.filter(todo => {
    //   return todo.id !== id
    // })
    // this.setState({
    //   todos
    // })
  }
  addTodo = (todo) => {
    // console.log(todo)
    todo.id = Math.random();
    this.props.addTodo(todo)
    // let todos = [...this.state.todos];
    // todos.unshift(todo)
    // this.setState({todos})
  }
  render() {
    // console.log(this.props)
    return (
      <div className="todo-app container">
        <h1 className="center blue-text">Todo's</h1>
        <AddTodo addTodo={this.addTodo} />
        <Todos todos={this.props.todo} deleteTodo={this.deleteTodo} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  // console.log(state)
  return {
    todo: state.todo
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteTodo : (id) => {dispatch (delTodoAction(id))},
    addTodo : (todo) => {dispatch (addTodoAction(todo))}
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
